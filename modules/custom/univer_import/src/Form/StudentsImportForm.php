<?php

namespace Drupal\univer_import\Form;

use Drupal\Core\TypedData\Type\DateTimeInterface;
use Drupal\node\Entity\Node;
use Drupal\file\Entity\File;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldConfig;

/**
 * Class StudentsImportForm.
 */
class StudentsImportForm extends FormBase {

  /**
   * @var integer csv column number.
   */
  const CSV_COLUMN_NUMBER = 47;

  const CSV_DELIMITER = ';';
  /**
   * @var array Field mapping.
   */
  protected $field_mapping = [
    'field_data_zavantazhennya' => [
      'csv_column' => 0,
      'type' => '',
    ],
    'field_id_kartki'  => [
      'csv_column' => 1,
      'type' => 'text',
    ],
    'field_status_z' => [
      'csv_column' => 2,
      'type' => 'text',
    ],
    'field_status_navchannya' => [
      'csv_column' => 3,
      'type' => 'text',
    ],
    'field_zdobuvach' => [
      'csv_column' => 4,
      'type' => 'text',
    ],
    'field_data_narodzhennya' => [
      'csv_column' => 5,
      'type' => 'date',
    ],
    'field_tip_dpo' => [
      'csv_column' => 6,
      'type' => 'text',
    ],
    'field_seriya_dokumenta' => [
      'csv_column' => 7,
      'type' => 'text',
    ],
    'field_nomer_dokumenta' => [
      'csv_column' => 8,
      'type' => 'text',
    ],
    'field_data_vidachi' => [
      'csv_column' => 9,
      'type' => 'date',
    ],
    'field_stat' => [
      'csv_column' => 10,
      'type' => 'text',
    ],
    'field_gr_ukraine' => [
      'csv_column' => 11,
      'type' => 'text',
    ],
    'field_pib_angliyskoyu' => [
      'csv_column' => 12,
      'type' => 'text',
    ],
    'field_rnokpp' => [
      'csv_column' => 13,
      'type' => 'text',
    ],
    'field_rik_licenziynikh_obsyagiv' => [
      'csv_column' => 14,
      'type' => 'text',
    ],
    'field_pochatok_navchannya' => [
      'csv_column' => 15,
      'type' => 'date',
    ],
    'field_zavershennya_navchannya' => [
      'csv_column' => 16,
      'type' => 'date',
    ],
    'field_strukturniy_pidrozdil' => [
      'csv_column' => 17,
      'type' => 'text',
    ],
    'field_osvitniy_stupin_riven' => [
      'csv_column' => 18,
      'type' => 'text',
    ],
    'field_vstup_na_osnovi' => [
      'csv_column' => 19,
      'type' => 'text',
    ],
    'field_forma_navchannya' => [
      'csv_column' => 20,
      'type' => 'text',
    ],
    'field_dzherelo_finansuvannya' => [
      'csv_column' => 21,
      'type' => 'text',
    ],
    'field_chi_zdobuv_stupen_special' => [
      'csv_column' => 22,
      'type' => 'text',
    ],
    'field_chi_skorocheniy_termin_nav' => [
      'csv_column' => 23,
      'type' => 'text',
    ],
    'field_specialnist' => [
      'csv_column' => 24,
      'type' => 'text',
    ],
    'field_specializaciya' => [
      'csv_column' => 25,
      'type' => 'text',
    ],
    'field_osvitnya_programa' => [
      'csv_column' => 26,
      'type' => 'text',
    ],
    'field_profesiya' => [
      'csv_column' => 27,
      'type' => 'text',
    ],
    'field_v_kartci_doc_pro_osvity' => [
      'csv_column' => 28,
      'type' => 'text',
    ],
    'field_ucnivskiy_kvutok' => [
      'csv_column' => 29,
      'type' => 'text',
    ],
    'field_akademichna_dovidka' => [
      'csv_column' => 30,
      'type' => 'text',
    ],
    'field_prichina_vidrakhuvannya' => [
      'csv_column' => 31,
      'type' => 'text',
    ],
    'field_pidstavi_nadannya_akademic' => [
      'csv_column' => 32,
      'type' => 'text',
    ],
    'field_status_po' => [
      'csv_column' => 33,
      'type' => 'text',
    ],
    'field_status_diploma_v_zam_doc' => [
      'csv_column' => 34,
      'type' => 'text',
    ],
    'field_status_student_kvut_v_zam' => [
      'csv_column' => 35,
      'type' => 'text',
    ],
    'field_status_svidoctva_pro_prisv' => [
      'csv_column' => 36,
      'type' => 'text',
    ],
    'field_rik_byudzhetu' => [
      'csv_column' => 37,
      'type' => 'text',
    ],
    'field_nakaz_pro_zarakhuvannya' => [
      'csv_column' => 38,
      'type' => 'text',
    ],
    'field_poperedniy_zaklad_osviti' => [
      'csv_column' => 39,
      'type' => 'text',
    ],
    'field_dokument_pro_osvitu' => [
      'csv_column' => 40,
      'type' => 'text',
    ],
    'field_informaciya_pro_poperedne_' => [
      'csv_column' => 41,
      'type' => 'text',
    ],
    'field_akademichna_dovidka_osvitn' => [
      'csv_column' => 42,
      'type' => 'text',
    ],
    'field_studentskiy_uchnivsk_nom' => [
      'csv_column' => 43,
      'type' => 'text',
    ],
    'field_vidaniy_diplom' => [
      'csv_column' => 44,
      'type' => 'text',
    ],
    'field_informaciya_pro_zarakhuvan' => [
      'csv_column' => 45,
      'type' => 'text',
    ],
    'field_chas_ostannoi_zmini' => [
      'csv_column' => 46,
      'type' => 'text',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'students_import_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
     $form['file'] = [
      '#type' => 'managed_file',
      '#title' => t('Upload csv file(s).'),
      '#description' => t('You should use csv file(s) with "," delimiter.'),
      '#multiple' => TRUE,
      '#upload_validators' => [
        'file_validate_extensions' => ['csv'],
      ],
      '#attributes' => ['multiple' => 'multiple'],
    ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * Import student.
   *
   * @param array $data
   *   Data for import.
   */
  protected function importStudent($data) {
    $student_id = $data[$this->field_mapping['field_id_kartki']['csv_column']];

    // Check if node exist.
    $query = \Drupal::entityQuery('node')
      ->condition('status', 1)
      ->condition('type', 'student')
      ->condition('field_id_kartki', $student_id);
    $result = $query->execute();


    if ($result) {
      $nid = reset($result);
      /** @var \Drupal\node\Entity\Node $student */
      $student = Node::load($nid);
    }
    else {
      /** @var \Drupal\node\Entity\Node $node */
      $student = Node::create([
        'type' => 'student',
        'title' => $student_id,
      ]);
    }

    $this->setStudentData($student, $data);
    $student->save();
  }

  /**
   * Fill student with information from import.
   *
   * @param \Drupal\node\NodeInterface $student
   *   Node.
   * @param array $data
   *   Data.
   */
  protected function setStudentData($student, $data) {
    foreach ($this->field_mapping as $field_name => $field_option) {
      // Check if csv data exists for column
      if (!isset($data[$field_option['csv_column']])) {
        continue;
      }

      // Encode value for field.
      $field_value = $this->encodeInputText($data[$field_option['csv_column']]);
      switch ($field_option['type']) {
        case 'text':
          $this->setFieldText($student, $field_name, $field_value);
          break;

        case 'date':
          $this->setFieldDate($student, $field_name, $field_value);
          break;
      }
    }
    \Drupal::messenger()->addMessage(t('Imported student with id: @student_id', [
      '@student_id' => $data[$this->field_mapping['field_id_kartki']['csv_column']],
    ]));
  }

  /**
   * Set text information.
   *
   * @param \Drupal\node\NodeInterface $student
   *   Student.
   * @param string $field_name
   *   File name.
   * @param string $field_value
   *   Data for field.
   */
  protected function setFieldText($student, $field_name, $field_value) {
    $student->set($field_name, $field_value);
  }

  /**
   * Set date information.
   *
   * @param \Drupal\node\NodeInterface $student
   *   Student.
   * @param string $field_name
   *   File name.
   * @param string $field_value
   *   Data for field.
   */
  protected function setFieldDate($student, $field_name, $field_value) {
    $date = strtotime($field_value);

    $date_object = date('Y-m-d', $date);
    $student->set($field_name, $date_object);
  }
  /**
   * Encode from windows-1251 to utf-8.
   *
   * @param string $text
   *   Input text.
   *
   * @return string
   *   Encoded text.
   */
  protected function encodeInputText($text) {
    $skip_coding = [
      'UTF-8'
    ];
    if (in_array(mb_detect_encoding($text), $skip_coding)) {
      return $text;
    }

    return mb_convert_encoding($text, 'utf-8', 'windows-1251');
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $files = $form_state->getValue('file');
    $messenger = \Drupal::messenger();

    foreach ($files as $fid) {
      /** @var \Drupal\file\Entity\File $file */
      $file = File::load($fid);
      if (fopen($file->getFileUri(), 'r') !== FALSE) {
        $handle = fopen($file->getFileUri(), 'r');

        $line_number = 0;
        while (($data = fgetcsv($handle, 0, self::CSV_DELIMITER)) !== FALSE) {
          // Skip header.
          if ($line_number == 0) {
            $line_number++;
            continue;
          }

          $line_number++;

          if (count($data) != self::CSV_COLUMN_NUMBER) {
            $messenger->addMessage(t('File should contain @number column', [
              '@number' => self::CSV_COLUMN_NUMBER,
            ]), $messenger::TYPE_ERROR);
          }
          else {
            $this->importStudent($data);
          }
        }
      }
    }

  }

}
